﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Files : MonoBehaviour
{
    private static List<string> data;
    public static string Str { get; set; }

    public static void Write(float time, float kill, string name)
    {

        if (!File.Exists("Assets/Records/records.ini"))
        { 
            File.Create("Assets/Records/records.ini");
            data = new List<string>();
        }
        Read();
        Str = time + "\n" + kill;
        foreach (var v in data)
        {
            Str += "\n" + v;
        }
        data.Add(time.ToString());
        data.Add(kill.ToString());
        File.WriteAllText("Assets/Records/records.ini", Str);
    }

    public static List<string> Read()
    {
        data = new List<string>();
        string text = File.ReadAllText("Assets/Records/records.ini");
        if (text.Length != 0)
        { 
            var dat = text.Split('\n');
            foreach(string v in dat)
                data.Add(v);
        }
        return data;
    }
}
