﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class RecordsList : MonoBehaviour
{
    [SerializeField] private RectTransform prefab;

    private void Start()
    {
        OnRoomListUpdate();
    }

    private void OnRoomListUpdate()
    {
        var datas = Files.Read();
        int size = datas.Count / 2, count = 0;
        if(datas != null)
            for (var i = 0; i < datas.Count; i++)
            {
                var instance = Instantiate(prefab.gameObject, gameObject.transform, false);
                var rec = instance.GetComponent<Recording>();
                rec.Writing("Try " + (size), datas[i + 1], datas[i]);
                i++;
                size--;
                count++;
                if (count == 3)
                    break;
            }
    }
}
