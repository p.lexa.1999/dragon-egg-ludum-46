﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Recording : MonoBehaviour
{
    [SerializeField] private GameObject tryText;
    [SerializeField] private GameObject killsText;
    [SerializeField] private GameObject timeText;

    public void Writing(string textTry, string textKills, string textTime)
    {
        tryText.GetComponent<Text>().text = textTry;
        killsText.GetComponent<Text>().text = textKills;
        timeText.GetComponent<Text>().text = textTime;
    }
}
