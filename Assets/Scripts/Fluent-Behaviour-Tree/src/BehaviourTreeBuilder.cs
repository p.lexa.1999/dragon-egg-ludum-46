﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Assertions;

namespace FluentBehaviourTree
{
    /// <summary>
    /// Fluent API for building a behaviour tree.
    /// </summary>
    public class BehaviourTreeBuilder
    {
        /// <summary>
        /// Last node created.
        /// </summary>
        private BhvNodeBase curNode = null;

        /// <summary>
        /// Stack node nodes that we are build via the fluent API.
        /// </summary>
        private Stack<BhvNodeParent> parentNodeStack = new Stack<BhvNodeParent>();

        private GameObject owner;

        public BehaviourTreeBuilder(GameObject owner)
        {
            this.owner = owner;
        }

        /// <summary>
        /// Create an action node.
        /// </summary>
        public BehaviourTreeBuilder Do(string name, Func<TimeData, BehaviourTreeStatus> fn) => AddLeafNode(new ActionNode(name, owner, fn));

        /// <summary>
        /// Like an action node... but the function can return true/false and is mapped to success/failure.
        /// </summary>
        public BehaviourTreeBuilder Condition(string name, Func<TimeData, bool> fn)
        {
            return Do(name, t => fn(t) ? BehaviourTreeStatus.Success : BehaviourTreeStatus.Failure);
        }

        /// <summary>
        /// Create an inverter node that inverts the success/failure of its children.
        /// </summary>
        public BehaviourTreeBuilder Inverter(string name) => AddParentNode(new InverterNode(name, owner));

        /// <summary>
        /// Create a sequence node.
        /// </summary>
        public BehaviourTreeBuilder Sequence(string name) => AddParentNode(new SequenceNode(name, owner));

        /// <summary>
        /// Create a parallel node.
        /// </summary>
        public BehaviourTreeBuilder Parallel(string name, int numRequiredToFail, int numRequiredToSucceed)
            => AddParentNode(new ParallelNode(name, owner, numRequiredToFail, numRequiredToSucceed));

        /// <summary>
        /// Create a selector node.
        /// </summary>
        public BehaviourTreeBuilder Selector(string name) => AddParentNode(new SelectorNode(name, owner));

        public BehaviourTreeBuilder Wait(string name, float waitSec)
        {
            Assert.IsTrue(waitSec > 0, "Wrong wait time: " + waitSec);
            var node = new BhvNodeWait(name, owner)
            {
                WaitSec = waitSec
            };
            return AddLeafNode(node);
        }

        public BehaviourTreeBuilder WaitRandom(string name, float waitRandomMinSec, float waitRandomMaxSec)
        {
            Assert.IsTrue(waitRandomMinSec > 0 && waitRandomMaxSec > 0, "Wrong wait random time range [" + waitRandomMinSec + ", " + waitRandomMaxSec + "]");
            Assert.IsTrue(waitRandomMinSec < waitRandomMaxSec, "Wrong wait random time range [" + waitRandomMinSec + ", " + waitRandomMaxSec + "]");
            var node = new BhvNodeWaitRandom(name, owner)
            {
                WaitRandomMinSec = waitRandomMinSec,
                WaitRandomMaxSec = waitRandomMaxSec
            };
            return AddLeafNode(node);
        }

        public BehaviourTreeBuilder GoToRandomPoint(float randomPointMinDist, float randomPointMaxDist, GameObject targetPrefab)
        {
            Assert.IsTrue(randomPointMinDist > 0 && randomPointMaxDist > 0, "Wrong distance range [" + randomPointMinDist + ", " + randomPointMaxDist + "]");
            Assert.IsTrue(randomPointMinDist < randomPointMaxDist, "Wrong distance range [" + randomPointMinDist + ", " + randomPointMaxDist + "]");
            Assert.IsNotNull(targetPrefab);

            var node = new BhvNodeGoToRandomPoint("", owner)
            {
                RandomPointMinDist = randomPointMinDist,
                RandomPointMaxDist = randomPointMaxDist,
                TargetPrefab = targetPrefab
            };
            return AddLeafNode(node);
        }

        public BehaviourTreeBuilder Roll(float chance)
        {
            Assert.IsTrue(
                (chance > 0f || Mathf.Approximately(chance, 0f)) && (chance < 1.0f || Mathf.Approximately(chance, 1.0f)), 
                "Wrong roll percent chance: " + chance + ". Must be [0, 1]"
            );
            var node = new BhvNodeRoll("", owner)
            {
                RollChance = chance
            };
            return AddLeafNode(node);
        }

        public BehaviourTreeBuilder CheckDistanceToEnemy(float minDist, float maxDist)
        {
            Assert.IsTrue(minDist < maxDist, "Wrong distance range [" + minDist + ", " + maxDist + "]");
            var node = new BhvNodeCheckDistanceToEnemy("", owner)
            {
                MinDist = minDist,
                MaxDist = maxDist
            };
            return AddLeafNode(node);
        }

        public BehaviourTreeBuilder IsDestinationReached() => AddLeafNode(new BhvNodeIsDestinationReached("", owner));
        public BehaviourTreeBuilder IsEnemyAlive()         => AddLeafNode(new BhvNodeIsEnemyAlive("", owner));
        public BehaviourTreeBuilder IsEnemyHasEnergy()     => AddLeafNode(new BhvNodeIsEnemyHasEnergy("", owner));
        public BehaviourTreeBuilder HasEnemy()             => AddLeafNode(new BhvNodeHasEnemy("", owner));
        public BehaviourTreeBuilder IsEnemyInAttackRange() => AddLeafNode(new BhvNodeIsEnemyInAttackRange("", owner));
        public BehaviourTreeBuilder Attack()               => AddLeafNode(new BhvNodeAttack("", owner));
        public BehaviourTreeBuilder IsEnemyReached()       => AddLeafNode(new BhvNodeIsEnemyReached("", owner));
        public BehaviourTreeBuilder GoToEnemy()            => AddLeafNode(new BhvNodeGoToEnemy("", owner));
        public BehaviourTreeBuilder ResetPath()            => AddLeafNode(new BhvNodeResetPath("", owner));
        public BehaviourTreeBuilder Fail()                 => AddLeafNode(new BhvNodeFail("", owner));
        public BehaviourTreeBuilder Success()              => AddLeafNode(new BhvNodeSuccess("", owner));

        private BehaviourTreeBuilder AddLeafNode(BhvNodeLeaf node)
        {
            if (parentNodeStack.Count <= 0)
            {
                throw new ApplicationException("Can't create an unnested node " + node.GetType().Name + ", it must be a leaf node.");
            }
            parentNodeStack.Peek().AddChild(node);
            return this;
        }

        private BehaviourTreeBuilder AddParentNode(BhvNodeParent node)
        {
            if (parentNodeStack.Count > 0)
            {
                parentNodeStack.Peek().AddChild(node);
            }

            parentNodeStack.Push(node);
            return this;
        }

        /// <summary>
        /// Splice a sub tree into the parent tree.
        /// </summary>
        public BehaviourTreeBuilder Splice(BhvNodeBase subTree)
        {
            if (subTree == null)
            {
                throw new ArgumentNullException("subTree");
            }

            if (parentNodeStack.Count <= 0)
            {
                throw new ApplicationException("Can't splice an unnested sub-tree, there must be a parent-tree.");
            }

            parentNodeStack.Peek().AddChild(subTree);
            return this;
        }

        /// <summary>
        /// Build the actual tree.
        /// </summary>
        public BhvNodeBase Build()
        {
            if (curNode == null)
            {
                throw new ApplicationException("Can't create a behaviour tree with zero nodes");
            }

            return curNode;
        }

        /// <summary>
        /// Ends a sequence of children.
        /// </summary>
        public BehaviourTreeBuilder End()
        {
            curNode = parentNodeStack.Pop();
            return this;
        }
    }
}
