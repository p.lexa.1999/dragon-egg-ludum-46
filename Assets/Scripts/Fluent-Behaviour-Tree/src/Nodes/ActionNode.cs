﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace FluentBehaviourTree
{
    /// <summary>
    /// A behaviour tree leaf node for running an action.
    /// </summary>
    public class ActionNode : BhvNodeLeaf
    {
        /// <summary>
        /// Function to invoke for the action.
        /// </summary>
        private Func<TimeData, BehaviourTreeStatus> fn;

        public ActionNode(string name, GameObject owner, Func<TimeData, BehaviourTreeStatus> fn)
            : base(name, owner)
        {
            this.fn=fn;
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            return fn(time);
        }
    }
}
