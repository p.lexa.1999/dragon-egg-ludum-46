﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Assertions;

namespace FluentBehaviourTree
{ 
    public abstract class BhvNodeBase
    {
        public virtual bool IsLoggingEnabled { get; set; }

        private int nestedLevel;
        public int NestedLevel
        {
            get => nestedLevel;
            set
            {
                Assert.IsTrue(value >= 0, "nested level can't be negative");
                this.nestedLevel = value;
            }
        }

        protected string name;
        protected GameObject owner;
        protected BhvTreeComponent bhvTreeComponent;

        public abstract BehaviourTreeStatus TickInternal(TimeData time);

        public BehaviourTreeStatus Tick(TimeData time)
        {
            TryLogTick();
            var res = TickInternal(time);
            TryLogTickResult(res);
            return res;
        }

        public BhvNodeBase(string name, GameObject owner)
        {
            this.name = name ?? "";
            Assert.IsNotNull(owner, "owner can't be null");
            this.owner = owner;
            this.bhvTreeComponent = owner.GetComponent<BhvTreeComponent>();
            Assert.IsNotNull(bhvTreeComponent, owner + " must have BhvTreeComponent");
            this.NestedLevel = 0;
        }

        protected void TryLogMsg(string msg)
        {
            if (IsLoggingEnabled)
            {
                Debug.Log(ToString() + ": " + msg, owner);
            }
        }

        protected void TryLogTick() => TryLogMsg("Tick");
        protected void TryLogTickResult(BehaviourTreeStatus result) => TryLogMsg("Tick result: " + result);

        public override string ToString()
        {
            return String.Format("bhvTree: {0}{1}@{2}{3}", 
                GetPadding(), 
                owner.name, 
                this.GetType().Name,
                "[" + name + "]"
            );
        }

        private string GetPadding()
        {
            return String.Concat(Enumerable.Repeat("- ", nestedLevel));
        }

        public virtual void OnDestroy()
        {
            // nothing to do here...
        }
    }
}
