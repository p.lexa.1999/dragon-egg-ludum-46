﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FluentBehaviourTree
{
    public abstract class BhvNodeLeaf : BhvNodeBase
    {
        public BhvNodeLeaf(string name, GameObject owner)
            : base(name, owner)
        {
        }
    }
}
