﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FluentBehaviourTree
{
    public abstract class BhvNodeParent : BhvNodeBase
    {
        public override bool IsLoggingEnabled
        {
            get => base.IsLoggingEnabled;
            set
            {
                base.IsLoggingEnabled = value;
                foreach (var child in children)
                {
                    child.IsLoggingEnabled = value;
                }
            }
        }

        protected List<BhvNodeBase> children = new List<BhvNodeBase>();

        public BhvNodeParent(string name, GameObject owner) 
            : base(name, owner)
        {
        }

        public virtual void AddChild(BhvNodeBase child)
        {
            child.NestedLevel = this.NestedLevel + 1;
            children.Add(child);
        }

        public override void OnDestroy()
        {
            foreach (var child in children)
            {
                child.OnDestroy();
            }

            base.OnDestroy();
        }
    }
}
