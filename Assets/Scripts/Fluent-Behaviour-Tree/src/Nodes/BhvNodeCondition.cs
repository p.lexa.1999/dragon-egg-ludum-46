﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FluentBehaviourTree
{
    public abstract class BhvNodeCondition : BhvNodeLeaf
    {

        public BhvNodeCondition(string name, GameObject owner)
            : base(name, owner)
        {
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            if (CheckCondition(time))
            {
                return BehaviourTreeStatus.Success;
            }
            return BehaviourTreeStatus.Failure;
        }

        protected abstract bool CheckCondition(TimeData time);
    }
}
