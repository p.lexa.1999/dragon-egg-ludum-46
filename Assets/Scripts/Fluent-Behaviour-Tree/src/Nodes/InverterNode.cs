﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace FluentBehaviourTree
{
    /// <summary>
    /// Decorator node that inverts the success/failure of its child.
    /// </summary>
    public class InverterNode : BhvNodeParent
    {
        public InverterNode(string name, GameObject owner)
            : base(name, owner)
        {
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            if (children.Count == 0)
            {
                throw new ApplicationException("InverterNode must have a child node!");
            }

            var result = children[0].Tick(time);
            if (result == BehaviourTreeStatus.Failure)
            {
                result = BehaviourTreeStatus.Success;
            }
            else if (result == BehaviourTreeStatus.Success)
            {
                result = BehaviourTreeStatus.Failure;
            }

            return result;
        }

        public override void AddChild(BhvNodeBase child)
        {
            if (children.Count != 0)
            {
                throw new ApplicationException("Can't add more than a single child to InverterNode!");
            }

            child.NestedLevel = NestedLevel + 1;
            children.Add(child);
        }
    }
}
