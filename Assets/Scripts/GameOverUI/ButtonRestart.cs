﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonRestart : MonoBehaviour
{
    private void OnMouseDown()
    {
        Time.timeScale = 1;
        
        SceneManager.LoadScene("Scenes/Gameplay");
    }
}
