﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class EndGame : MonoBehaviour
{
    [SerializeField] private GameObject endView;
    [SerializeField] private GameObject timeText;
    [SerializeField] private GameObject killText;
    [SerializeField] private GameObject restart;
    [SerializeField] private GameObject records;

    private void Start()
    {
        GameManager.Instance.OnEndGame += End;
    }

    private void End()
    {
        endView.SetActive(true);
        records.GetComponent<Collider2D>().enabled = false;
        records.GetComponent<Collider2D>().enabled = true;
        restart.GetComponent<Collider2D>().enabled = false;
        restart.GetComponent<Collider2D>().enabled = true;
        GameManager.Instance.player.GetComponent<InputController>().enabled = false;
        timeText.GetComponent<Text>().text = GameManager.Instance.GameTime.ToString();
        killText.GetComponent<Text>().text = GameManager.Instance.Kills.ToString();
        Time.timeScale = 0;
    }

}
