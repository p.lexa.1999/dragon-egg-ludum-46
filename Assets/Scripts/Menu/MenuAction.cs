﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuAction : MonoBehaviour
{
    [SerializeField] private GameObject record;
    [SerializeField] private GameObject text;
    [SerializeField] private GameObject button;
    public void StartAction()
    {
        SceneManager.LoadScene("Scenes/Gameplay");
    }

    public void RecordsAction() => SceneManager.LoadScene("Scenes/Records");
    public void ExitAction() => Application.Quit();

    public void PlayAction()
    {
        record.SetActive(true);
        text.SetActive(true);
        button.SetActive(true);
    }
}
