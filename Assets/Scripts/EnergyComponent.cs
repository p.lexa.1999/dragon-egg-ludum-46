﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class EnergyComponent : HealthComponentBase
{
    [SerializeField] private float addEnergy;
    [SerializeField] private float periodAddEnergy;
    private int energ;

    private bool UsingFire { get; set; } = false;
    private DragonAttack _attack;
    public override void Start()
    {
        base.Start();
        _attack = GetComponent<DragonAttack>();
        StartCoroutine(AddEnergy());
    }

    private void Update()
    {
        energ =(int)GetHealth();
    }

    private IEnumerator AddEnergy()
    {
        yield return  new WaitForSeconds(.5f);
        while (!_attack.UsingFire)
        {
            RestoreHealth(addEnergy);
            yield return new WaitForSeconds(periodAddEnergy); 
        }
    }

    private IEnumerator DrainEnergy()
    {
        while(_attack.UsingFire)
        {
            if(!_attack.CanUseFire())
            {
                _attack.KeyUp();
            }
            TakeDamage(_attack.ReturnFireCost);
            yield return new WaitForSeconds(_attack.ReturnFirePeriod);
        }
    }

    public Coroutine StartDrain() => StartCoroutine(DrainEnergy());
    public void StopDrain(Coroutine drain) => StopCoroutine(drain);
    public Coroutine StartAddEnergy() => StartCoroutine(AddEnergy());
    public void StopAdd(Coroutine add) => StopCoroutine(add);

}
