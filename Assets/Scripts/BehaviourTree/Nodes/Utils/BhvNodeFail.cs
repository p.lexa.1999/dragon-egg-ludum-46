﻿using UnityEngine;

namespace FluentBehaviourTree
{
    class BhvNodeFail : BhvNodeLeaf
    {
        public BhvNodeFail(string name, GameObject owner)
            : base(name, owner)
        {
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            return BehaviourTreeStatus.Failure;
        }
    }
}
