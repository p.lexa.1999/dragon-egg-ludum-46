﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FluentBehaviourTree
{
    public class BhvNodeRoll : BhvNodeCondition
    {
        public float RollChance { get; set; }

        public BhvNodeRoll(string name, GameObject owner)
            : base(name, owner)
        {
        }

        protected override bool CheckCondition(TimeData time)
        {
            return UnityEngine.Random.value < RollChance;
        }
    }
}
