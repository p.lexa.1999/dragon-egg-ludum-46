﻿using UnityEngine;

namespace FluentBehaviourTree
{
    public class BhvNodeWait : BhvNodeLeaf
    {
        public float WaitSec { get; set; }

        private float waitTimeCurrSec = -1.0f;
        private float waitTimeTargetSec = -1.0f;

        public BhvNodeWait(string name, GameObject owner)
            : base(name, owner)
        {
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            if (waitTimeCurrSec < 0)
            {
                waitTimeCurrSec = 0.0f;
            }

            waitTimeCurrSec += time.deltaTime;
            if (waitTimeCurrSec > WaitSec)
            {
                waitTimeCurrSec = -1.0f;
                return BehaviourTreeStatus.Success;
            }
            return BehaviourTreeStatus.Running;
        }
    }
}
