﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FluentBehaviourTree
{
    class BhvNodeSuccess : BhvNodeLeaf
    {
        public BhvNodeSuccess(string name, GameObject owner)
            : base(name, owner)
        {
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            return BehaviourTreeStatus.Success;
        }

    }
}
