﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace FluentBehaviourTree
{
    public class BhvNodeWaitRandom : BhvNodeLeaf
    {
        public float WaitRandomMinSec { get; set; }
        public float WaitRandomMaxSec { get; set; }

        private float waitTimeCurrSec = -1.0f;
        private float waitTimeTargetSec = -1.0f;

        public BhvNodeWaitRandom(string name, GameObject owner)
            : base(name, owner)
        {
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            if (waitTimeCurrSec < 0 || waitTimeTargetSec < 0)
            {
                waitTimeCurrSec = 0.0f;
                waitTimeTargetSec = Random.Range(WaitRandomMinSec, WaitRandomMaxSec);
            }

            waitTimeCurrSec += time.deltaTime;
            if (waitTimeCurrSec > waitTimeTargetSec)
            {
                waitTimeCurrSec = -1.0f;
                waitTimeTargetSec = -1.0f;
                return BehaviourTreeStatus.Success;
            }
            return BehaviourTreeStatus.Running;
        }
    }
}
