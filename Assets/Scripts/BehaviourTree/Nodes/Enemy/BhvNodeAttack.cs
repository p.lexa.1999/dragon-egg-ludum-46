﻿using UnityEngine;
using UnityEngine.Assertions;

namespace FluentBehaviourTree
{
    class BhvNodeAttack: BhvNodeLeaf
    {
        private AttackComponent attackComponent;

        public BhvNodeAttack(string name, GameObject owner)
            : base(name, owner)
        {
            attackComponent = owner.GetComponent<AttackComponent>();
            Assert.IsNotNull(attackComponent, owner.name + ": must have AttackComponent");
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            if (attackComponent.IsAttackLocked)
            {
                return BehaviourTreeStatus.Failure;
            }

            attackComponent.Attack();
            return BehaviourTreeStatus.Success;
        }
    }
}
