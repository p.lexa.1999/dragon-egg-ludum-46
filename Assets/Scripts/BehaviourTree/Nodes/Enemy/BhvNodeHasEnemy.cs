﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace FluentBehaviourTree
{
    class BhvNodeHasEnemy : BhvNodeCondition
    {
        private EnemySelectionBase enemySelection;

        public BhvNodeHasEnemy(string name, GameObject owner)
            : base(name, owner)
        {
            enemySelection = owner.GetComponent<EnemySelectionBase>();
            Assert.IsNotNull(enemySelection, owner.name + ": must have EnemySelectionBase");
        }

        protected override bool CheckCondition(TimeData time) => enemySelection.HasEnemy;
    }
}
