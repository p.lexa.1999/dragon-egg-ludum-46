﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using Pathfinding;

namespace FluentBehaviourTree
{
    class BhvNodeGoToEnemy : BhvNodeLeaf
    {
        private EnemySelection enemySelection;
        private AIDestinationSetter destinationSetter;

        public BhvNodeGoToEnemy(string name, GameObject owner)
            : base(name, owner)
        {
            enemySelection = owner.GetComponent<EnemySelection>();
            Assert.IsNotNull(enemySelection, owner.name + ": must have EnemySelection");
            destinationSetter = owner.GetComponent<AIDestinationSetter>();
            Assert.IsNotNull(destinationSetter, owner.name + ": must have AIDestinationSetter");
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            if (enemySelection.targetOverride != null)
            {
                destinationSetter.target = enemySelection.targetOverride.transform;
                return BehaviourTreeStatus.Success;
            }

            if (!enemySelection.HasEnemy)
            {
                return BehaviourTreeStatus.Failure;
            }

            destinationSetter.target = enemySelection.Enemy.transform;
            return BehaviourTreeStatus.Success;
        }
    }
}
