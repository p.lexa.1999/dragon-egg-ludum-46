﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using Pathfinding;

namespace FluentBehaviourTree
{
    class BhvNodeIsEnemyInAttackRange : BhvNodeCondition
    {
        private AIDestinationSetter destinationSetter;
        private EnemySelectionBase enemySelection;
        private AttackComponent attackComponent;

        public BhvNodeIsEnemyInAttackRange(string name, GameObject owner)
            : base(name, owner)
        {
            enemySelection = owner.GetComponent<EnemySelectionBase>();
            Assert.IsNotNull(enemySelection, owner.name + ": must have EnemySelectionBase");
            destinationSetter = owner.GetComponent<AIDestinationSetter>();
            Assert.IsNotNull(destinationSetter, owner.name + ": must have AIDestinationSetter");
            attackComponent = owner.GetComponent<AttackComponent>();
            Assert.IsNotNull(attackComponent, owner.name + ": must have AttackComponent");
        }

        protected override bool CheckCondition(TimeData time)
        {
            if (!enemySelection.HasEnemy)
            {
                return false;
            }
            var posA = new Vector2(owner.transform.position.x, owner.transform.position.y);
            var posB = new Vector2(enemySelection.Enemy.transform.position.x, enemySelection.Enemy.transform.position.y);
            var distance = Vector2.Distance(posA, posB) - 3;
            var res = distance < attackComponent.AttackRange ||
                Mathf.Approximately(distance, attackComponent.AttackRange);
            return res;
        }
    }
}
