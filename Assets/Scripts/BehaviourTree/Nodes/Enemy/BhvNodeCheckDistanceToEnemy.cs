﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;

namespace FluentBehaviourTree
{
    class BhvNodeCheckDistanceToEnemy : BhvNodeCondition
    {
        public float MinDist { get; set; }
        public float MaxDist { get; set; }

        private NavMeshAgent navmeshAgent;
        private EnemySelectionBase enemySelection;

        public BhvNodeCheckDistanceToEnemy(string name, GameObject owner)
            : base(name, owner)
        {
            enemySelection = owner.GetComponent<EnemySelectionBase>();
            Assert.IsNotNull(enemySelection, owner.name + ": must have EnemySelectionBase");
            navmeshAgent = owner.GetComponent<NavMeshAgent>();
            Assert.IsNotNull(enemySelection, owner.name + ": must have NavMeshAgent");
        }

        protected override bool CheckCondition(TimeData time)
        {
            if (!enemySelection.HasEnemy)
            {
                return false;
            }
            var posA = new Vector2(owner.transform.position.x, owner.transform.position.y);
            var posB = new Vector2(enemySelection.Enemy.transform.position.x, enemySelection.Enemy.transform.position.y);
            var distance = Vector2.Distance(posA, posB);
            return (distance < MaxDist || Mathf.Approximately(distance, MaxDist)) && 
                (distance > MinDist || Mathf.Approximately(distance, MinDist));
        }
    }
}
