﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace FluentBehaviourTree
{
    public class BhvNodeIsEnemyAlive : BhvNodeCondition
    {
        private EnemySelectionBase enemySelection;
        public BhvNodeIsEnemyAlive(string name, GameObject owner)
            : base(name, owner)
        {
            enemySelection = owner.GetComponent<EnemySelectionBase>();
            Assert.IsNotNull(enemySelection, owner.name + ": must have EnemySelectionBase");
        }

        protected override bool CheckCondition(TimeData time)
        {
            if (!enemySelection.HasEnemy)
            {
                return false;
            }
            var health = enemySelection.Enemy.GetComponent<HealthComponent>();
            return health?.IsAlive ?? false;
        }
    }

}