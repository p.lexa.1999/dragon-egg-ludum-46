﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Assertions;

namespace FluentBehaviourTree
{
    public class BhvNodeIsEnemyHasEnergy : BhvNodeCondition
    {
        private EnemySelectionBase enemySelection;

        public BhvNodeIsEnemyHasEnergy(string name, GameObject owner)
            : base(name, owner)
        {
            enemySelection = owner.GetComponent<EnemySelectionBase>();
            Assert.IsNotNull(enemySelection, owner.name + ": must have EnemySelectionBase");
        }

        protected override bool CheckCondition(TimeData time)
        {
            if (!enemySelection.HasEnemy)
            {
                return false;
            }
            var energy = enemySelection.Enemy.GetComponent<EnergyComponent>();
            return energy?.IsAlive ?? false;
        }
    }
}
