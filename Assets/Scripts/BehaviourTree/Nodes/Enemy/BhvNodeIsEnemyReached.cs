﻿using System;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using Pathfinding;

namespace FluentBehaviourTree
{
    class BhvNodeIsEnemyReached : BhvNodeCondition
    {
        private AIPath aiPath;
        private EnemySelectionBase enemySelection;

        public BhvNodeIsEnemyReached(string name, GameObject owner)
            : base(name, owner)
        {
            enemySelection = owner.GetComponent<EnemySelectionBase>();
            Assert.IsNotNull(enemySelection, owner.name + ": must have EnemySelectionBase");
            aiPath = owner.GetComponent<AIPath>();
            Assert.IsNotNull(aiPath, owner.name + ": must have AIPath");
        }

        protected override bool CheckCondition(TimeData time)
        {
            if (!enemySelection.HasEnemy)
            {
                return true;
            }
            var posA = new Vector2(owner.transform.position.x, owner.transform.position.y);
            var posB = new Vector2(enemySelection.Enemy.transform.position.x, enemySelection.Enemy.transform.position.y);
            var distance = Vector2.Distance(posA, posB);
            var res = distance < aiPath.endReachedDistance ||
                   Mathf.Approximately(distance, aiPath.endReachedDistance);
            return res;
        }
    }
}
