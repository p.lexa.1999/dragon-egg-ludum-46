﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using Pathfinding;

namespace FluentBehaviourTree
{
    public class BhvNodeResetPath : BhvNodeLeaf
    {
        private AIDestinationSetter destinationSetter;

        public BhvNodeResetPath(string name, GameObject owner)
            : base(name, owner)
        {
            destinationSetter = owner.GetComponent<AIDestinationSetter>();
            Assert.IsNotNull(destinationSetter, owner.name + ": must have AIDestinationSetter");
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            destinationSetter.target = null;
            return BehaviourTreeStatus.Success;
        }
    }
}
