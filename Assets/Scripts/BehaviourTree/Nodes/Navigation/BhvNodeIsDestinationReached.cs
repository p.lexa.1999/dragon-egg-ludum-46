﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using Pathfinding;

namespace FluentBehaviourTree
{
    public class BhvNodeIsDestinationReached : BhvNodeCondition
    {
        private AIPath aiPath;
        private AIDestinationSetter destinationSetter;

        public BhvNodeIsDestinationReached(string name, GameObject owner)
            : base(name, owner)
        {
            aiPath = owner.GetComponent<AIPath>();
            Assert.IsNotNull(aiPath, owner.name + ": must have AIPath");
            destinationSetter = owner.GetComponent<AIDestinationSetter>();
            Assert.IsNotNull(destinationSetter, owner.name + ": must have AIDestinationSetter");
        }

        protected override bool CheckCondition(TimeData time)
        {
            if (destinationSetter.target == null)
            {
                return true;
            }
            var res = aiPath.remainingDistance < aiPath.endReachedDistance ||
                           Mathf.Approximately(aiPath.remainingDistance, aiPath.endReachedDistance);
            return res;
        }
    }
}
