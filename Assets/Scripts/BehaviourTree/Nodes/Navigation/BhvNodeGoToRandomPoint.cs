﻿
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using Pathfinding;

namespace FluentBehaviourTree
{
    class BhvNodeGoToRandomPoint : BhvNodeLeaf
    {
        public float RandomPointMinDist { get; set; }
        public float RandomPointMaxDist { get; set; }
        public GameObject TargetPrefab { get; set; }

        private AIDestinationSetter destinationSetter;

        public BhvNodeGoToRandomPoint(string name, GameObject owner)
            : base(name, owner)
        {
            destinationSetter = owner.GetComponent<AIDestinationSetter>();
            Assert.IsNotNull(destinationSetter, owner.name + ": must have AIDestinationSetter");
        }

        public override BehaviourTreeStatus TickInternal(TimeData time)
        {
            var distance = Random.Range(RandomPointMinDist, RandomPointMaxDist);
            var positionOffset = Random.insideUnitSphere.normalized;
            positionOffset.z = 0;
            positionOffset = positionOffset * distance;
            var targetPos = owner.transform.position + positionOffset;
            var obj = GameObject.Instantiate(TargetPrefab);
            obj.transform.position = targetPos;
            destinationSetter.target = obj.transform;
            return BehaviourTreeStatus.Success;
        }
    }
}
