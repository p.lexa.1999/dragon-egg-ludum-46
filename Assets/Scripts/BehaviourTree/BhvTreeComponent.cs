﻿using UnityEngine;
using FluentBehaviourTree;
using UnityEngine.Assertions;
using UnityEditor;

public class BhvTreeComponent : MonoBehaviour
{
    [SerializeField] private BhvTreeSettingsBase bhvTreeSettings;

    private BhvNodeBase behaviourTree;
    private TimeData timeData = new TimeData();
    public bool IsUpdating { get; private set; }

    [Tooltip("Prints a lot of logs, like A LOT.\nMake sure to disable it if you dont need it.")]
    [SerializeField]
    private bool isLoggingEnabed;

    public void StartUpdating()
    {
        IsUpdating = true;
    }

    public void StopUpdating()
    {
        IsUpdating = false;
    }

    private void Awake()
    {
        Assert.IsTrue(
            GetComponents<BhvTreeComponent>().Length == 1, 
            gameObject + "  must have only one behaviour, but found " + GetComponents<BhvTreeComponent>().Length
        );
        behaviourTree = BhvTreeFactory.Create(gameObject, bhvTreeSettings);
        behaviourTree.IsLoggingEnabled = isLoggingEnabed;
        Assert.IsNotNull(behaviourTree, "behaviourTree must not be null");
        StartUpdating();
    }

    private void Update()
    {
        if (behaviourTree.IsLoggingEnabled != isLoggingEnabed)
        {
            behaviourTree.IsLoggingEnabled = isLoggingEnabed;
        }
        if (IsUpdating)
        {
            timeData.deltaTime = Time.deltaTime;
            behaviourTree.Tick(timeData);
        }
    }

    private void OnDestroy()
    {
        behaviourTree.OnDestroy();
    }

}
