﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEngine.Assertions;

namespace FluentBehaviourTree
{
    public static class BhvTreeFactory
    {
        private static Dictionary<string, Type> bhvTreeCreatorTypes;
        private static bool IsInitialized => bhvTreeCreatorTypes != null;

        private static void Initialize()
        {
            if (IsInitialized)
            {
                return;
            }

            // reflection magic
            var allTypes = Assembly.GetAssembly(typeof(BhvTreeCreatorBase<BhvTreeSettingsBase>)).GetTypes();
            var bhvCreatorTypes = allTypes.Where(
                type => type.IsClass && !type.IsAbstract && IsSubclassOfRawGeneric(typeof(BhvTreeCreatorBase<>), type)
            ).ToArray();

            bhvTreeCreatorTypes = new Dictionary<string, Type>();
            foreach (var type in bhvCreatorTypes)
            {
                dynamic temp = Activator.CreateInstance(type);
                bhvTreeCreatorTypes.Add(temp.Name, type);
            }
        }

        static bool IsSubclassOfRawGeneric(Type generic, Type toCheck)
        {
            while (toCheck != null && toCheck != typeof(object))
            {
                var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
                if (generic == cur)
                {
                    return true;
                }
                toCheck = toCheck.BaseType;
            }
            return false;
        }

        public static BhvNodeBase Create(GameObject owner, BhvTreeSettingsBase settings)
        {
            if (!IsInitialized)
            {
                Initialize();
            }

            Assert.IsTrue(
                bhvTreeCreatorTypes.ContainsKey(settings.bhvTreeName),
                "Can't find bhv tree with name " + settings.bhvTreeName
            );

            dynamic bhvTreeCreator = Activator.CreateInstance(bhvTreeCreatorTypes[settings.bhvTreeName]);
            dynamic bhvTreeSettingsCasted = settings;

            return bhvTreeCreator.CreateBhvTree(owner, bhvTreeSettingsCasted);
        }
    }
}
