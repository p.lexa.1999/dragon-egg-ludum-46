﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FluentBehaviourTree
{
    public class BhvTreeSettingsBase : ScriptableObject
    {
        public string bhvTreeName;
    }
}
