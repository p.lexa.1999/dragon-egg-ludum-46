﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FluentBehaviourTree
{
    public class BhvTreeCreatorWander : BhvTreeCreatorBase<BhvTreeSettingsWander>
    {
        public override string Name => "BhvTreeWander";

        public override BhvNodeBase CreateBhvTree(GameObject owner, BhvTreeSettingsWander settings)
        {
            return new BehaviourTreeBuilder(owner)
                .Sequence("")
                    .IsDestinationReached()
                    .ResetPath()
                    .WaitRandom("", settings.waitRandomMinSec, settings.waitRandomMaxSec)
                    .GoToRandomPoint(settings.randomPointMinDist, settings.randomPointMaxDist, settings.targetPrefab)
                .End()
                .Build();
        }
    }
}
