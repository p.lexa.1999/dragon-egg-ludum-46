﻿using UnityEngine;

namespace FluentBehaviourTree
{
    [CreateAssetMenu(menuName = "BhvTree/Settings/Wander", fileName = "Bhv Tree Settings Wander")]
    public class BhvTreeSettingsWander : BhvTreeSettingsBase
    {
        public float waitRandomMinSec = 1.0f;
        public float waitRandomMaxSec = 10.0f;

        public float randomPointMinDist = 2.0f;
        public float randomPointMaxDist = 10.0f;

        public GameObject targetPrefab;
    }
}
