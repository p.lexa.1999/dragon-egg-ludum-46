﻿using UnityEngine;

namespace FluentBehaviourTree
{
    [CreateAssetMenu(menuName = "BhvTree/Settings/AttackEnemy", fileName = "Bhv Tree Settings Attack Enemy")]
    public class BhvTreeSettingsAttackEnemy : BhvTreeSettingsBase
    {
        public float attackCooldownSec = 1.0f;
    }
}
