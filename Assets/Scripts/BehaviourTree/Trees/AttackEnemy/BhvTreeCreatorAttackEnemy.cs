﻿using UnityEngine;

namespace FluentBehaviourTree
{
    public class BhvTreeCreatorAttackEnemy : BhvTreeCreatorBase<BhvTreeSettingsAttackEnemy>
    {
        public override string Name => "BhvTreeAttackEnemy";

        public override BhvNodeBase CreateBhvTree(GameObject owner, BhvTreeSettingsAttackEnemy settings)
        { 
            // means parallel node never succeeds or fails and will run infinite
            const int PARALLEL_SUCC_CHILDREN_AMOUNT = int.MaxValue;
            const int PARALLEL_FAIL_CHILDREN_AMOUNT = int.MaxValue;

            return new BehaviourTreeBuilder(owner)
                .Parallel("", PARALLEL_SUCC_CHILDREN_AMOUNT, PARALLEL_FAIL_CHILDREN_AMOUNT)
                    .Splice(CreateLocomotionBhvTree(owner, settings))
                    .Splice(CreateBattleBhvTree(owner, settings))
                .End()
                .Build();
        }

        private BhvNodeBase CreateLocomotionBhvTree(GameObject owner, BhvTreeSettingsAttackEnemy settings)
        {
            return new BehaviourTreeBuilder(owner)
                .Selector("Locomotion")
                    .Sequence("NoEnemy")
                        .Inverter("")
                            .HasEnemy()
                        .End()
                        // do nothing
                    .End()
                    .Sequence("HasEnemy")
                        .HasEnemy()
                        .GoToEnemy()
                    .End()
                .End()
                .Build();
        }

        private BhvNodeBase CreateBattleBhvTree(GameObject owner, BhvTreeSettingsAttackEnemy settings)
        {
            return new BehaviourTreeBuilder(owner)
                .Selector("Battle")
                    .Sequence("TryAttackSequence")
                        .HasEnemy()
                        .IsEnemyAlive()
                        .IsEnemyInAttackRange()
                        .Wait("IsAttackOnCooldown", settings.attackCooldownSec)
                        .Attack()
                    .End()
                .End()
                .Build();
        }
    }
}
