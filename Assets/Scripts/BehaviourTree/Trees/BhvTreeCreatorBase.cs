﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FluentBehaviourTree
{
    public abstract class BhvTreeCreatorBase<T>
        where T : BhvTreeSettingsBase
    {
        public abstract string Name { get; }
        public abstract BhvNodeBase CreateBhvTree(GameObject owner, T settings);
    }
}
