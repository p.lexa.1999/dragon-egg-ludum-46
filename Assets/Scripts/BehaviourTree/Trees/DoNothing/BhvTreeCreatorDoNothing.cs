﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace FluentBehaviourTree
{
    public class BhvTreeCreatorDoNothing : BhvTreeCreatorBase<BhvTreeSettingsBase>
    {
        public override string Name => "BhvTreeDoNothing";

        public override BhvNodeBase CreateBhvTree(GameObject owner, BhvTreeSettingsBase bhvTreeParams)
        {
            return new BehaviourTreeBuilder(owner)
                .Sequence("my-sequence")
                    .Do("action1", t =>
                    {
                        return BehaviourTreeStatus.Success;
                    })
                .End()
                .Build();
        }
    }
}
