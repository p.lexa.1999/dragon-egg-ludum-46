﻿using UnityEngine;

namespace FluentBehaviourTree
{
    [CreateAssetMenu(menuName = "BhvTree/Settings/DoNothing", fileName = "Bhv Tree Settings Do Nothing")]
    public class BhvTreeSettingsDoNothing : BhvTreeSettingsBase
    {
    }
}
