﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEffect : MonoBehaviour
{
    [SerializeField] float speed = 30;
    [SerializeField] float lifeTime;
    [SerializeField] GameObject particle;

    [SerializeField] float startSize;
    [SerializeField] float endSize;
    [SerializeField] Color startColor;
    [SerializeField] Color endColor;
    float timeLeft;
    Renderer renderer;
    Color currentColor;
    public bool isActive = false;
    float time = 1;
    float timer = 1;
    List<GameObject> list = new List<GameObject>();
    [SerializeField] int maxSize;
    int i = 0;
    GameObject p;
    public Vector3 direction;

    FireEffectParticle eff;
    
    void Start()
    {
        for(int j = 0; j < maxSize; j++)
        {
            var pos = transform.position;
            p = Instantiate(particle, pos, Quaternion.Euler(0, 0, Random.Range(0, 360)));
            p.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            list.Add(p);
            var rb = p.AddComponent<Rigidbody2D>();
            eff = p.AddComponent<FireEffectParticle>();
        }
    }
    
    void FixedUpdate()
    {
        if (isActive)
        {
            Vector3 direction = GameManager.Instance.player.GetComponent<MovementController>().direction;
            var pos = transform.position;
            Debug.Log(list.Count);
            p = list[i];
            p.transform.position = pos;
            eff = p.GetComponent<FireEffectParticle>();
            eff.timeLeft = 0;

            eff.lifeTime = lifeTime;
            eff.startColor = startColor;
            eff.endColor = endColor;
            eff.startSize = startSize;
            eff.endSize = endSize;
            p.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            var localRb = p.GetComponent<Rigidbody2D>();
            localRb.velocity = Vector2.zero;
            localRb.gravityScale = 0;

            var coef = 1;
            

            localRb.AddForce((transform.up + direction * coef)* speed);
            i++;
            if (i == maxSize) i = 0;
        }
    }

    public void Play()
    {
        isActive = true;
    }

    public void Stop()
    {
        isActive = false;
    }

}
