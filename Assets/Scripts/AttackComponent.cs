﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackComponent : MonoBehaviour
{
    [SerializeField] AttackSettings attackSettings;

    private Vector2 OverlapPos
    {
        get
        {
            var fwd = Vector3.Scale(transform.forward, new Vector3(1, 1, 0)).normalized;
            var overlapPos = new Vector2(transform.position.x, transform.position.y) + (new Vector2(fwd.x, fwd.y) * 1.2f);
            return overlapPos;
        }
    }

    // approximate attack range, seems ok for now
    public float AttackRange => Mathf.Max(attackSettings.overlapSize.y, attackSettings.overlapSize.x);

    public int LastAttackStartFrame { get; private set; }

    public bool IsAttackLocked => false;

    private bool IsAttackRequested = false;

    public void Attack()
    {
        if (IsAttackLocked)
        {
            return;
        }
        IsAttackRequested = true;
        GetComponent<PeasantAnimController>()?.PlayAttackAnim();
    }

    private void DealDamage()
    {
        if (!IsAttackRequested)
        {
            return;
        }
        IsAttackRequested = false;

        var rangeAttack = GetComponent<RangeAttack>();
        if (rangeAttack != null)
        {
            rangeAttack.TrowProjectile();
            return;
        }

        LastAttackStartFrame = Time.frameCount;
        Physics.SyncTransforms();
        Collider2D[] enemys =  Physics2D.OverlapBoxAll(OverlapPos, attackSettings.overlapSize, transform.eulerAngles.z);
        foreach (Collider2D col in enemys)
        {
            if (col.gameObject != GameManager.Instance.Target)
            {
                continue;
            }

            var health = col.gameObject.GetComponent<HealthComponent>();
            if (health != null)
            {
                if (health.IsAlive)
                {
                    health.TakeDamage(attackSettings.damage, gameObject);
                }
                continue;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (isActiveAndEnabled)
        {
            Gizmos.color = Color.red;
            Gizmos.matrix = Matrix4x4.TRS(OverlapPos, transform.localRotation, attackSettings.overlapSize);
            Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
        }
    }
}

[Serializable]
public class AttackSettings
{
    public Vector2 overlapSize = new Vector2(2.5f, 1.7f);

    [Min(0)]
    public int damage = 10;
}
