﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggShield : MonoBehaviour
{
    HealthComponent health;

    void Start()
    {
        health = GetComponent<HealthComponent>();
        health.OnDeath += HandleDeath;
    }

    private void HandleDeath(GameObject killer)
    {
        Destroy(this.gameObject);
    }

    private void OnDestroy()
    {
        if (health != null)
        {
            health.OnDeath -= HandleDeath;
        }
    }
}
