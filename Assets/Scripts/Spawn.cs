﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    [SerializeField] [Min(0)] private float timeForStartSpawnSec = 0;
    [SerializeField] private float delayBetweenSpawn;
    [SerializeField] private GameObject enemyType;
    [SerializeField] private double coeff;
    [SerializeField] private bool alwaysSpawnOne;

    [SerializeField]
    [Tooltip("Заспавленные противники распределяются по указанным точкам")]
    GameObject[] targetsRotation;
    private int targetIdx = 0;

    private float _timeStart;
    private void Start()
    {
        StartCoroutine(SpawnEnemy());
        _timeStart = Time.timeSinceLevelLoad;
        var shield = GameObject.FindWithTag("eggShield");
        var shieldHealth = shield?.GetComponent<HealthComponent>();
        if (shieldHealth != null)
        {
            shieldHealth.OnDeath += HandleDeath;
        }
    }
    
    void HandleDeath(GameObject killer)
    {
        Destroy(this.gameObject);
    }

    private IEnumerator SpawnEnemy()
    {
        
        while (true)
        {
            if (Time.timeSinceLevelLoad < timeForStartSpawnSec)
            {
                yield return new WaitForSeconds(0.1f);
                continue;
            }

            int amountSpawn = alwaysSpawnOne ? 1 : (int)((Time.time - _timeStart) * coeff);
            
            for (int i = 0; i != amountSpawn; i++)
            {
                var enemy = Instantiate(enemyType, transform.position, Quaternion.identity);
                var enemySelection = enemy.GetComponent<EnemySelection>();
                if (enemySelection != null && targetsRotation.Length > 0)
                {
                    enemySelection.targetOverride = targetsRotation[targetIdx];
                    targetIdx++;
                    if (targetIdx >= targetsRotation.Length)
                    {
                        targetIdx = 0;
                    }
                }
            }
                
            yield return new WaitForSeconds(delayBetweenSpawn);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawCube(transform.position, Vector3.one);
    }
}