﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : HealthComponentBase
{
    [SerializeField] private int currentHealth;

    private void Update()
    {
        currentHealth = (int)GetHealth();
    }
}
