﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragonAttack : MonoBehaviour
{
    public bool UsingFire { get; private set; }
    [SerializeField] private float fireCost;
    [SerializeField] private float firePeriod;
    private EnergyComponent energy;
    private Coroutine drain, add;
    public float ReturnFireCost => fireCost;
    public float ReturnFirePeriod => firePeriod;
    [SerializeField] float attackBubleLifeTime;
    [SerializeField] GameObject dragonPihPrefab;
    [SerializeField] Transform attackProsition;
    [SerializeField] float speed;
    private Vector3 direction;

    private void Start()
    {
        energy = GetComponent<EnergyComponent>();
    }

    public bool CanUseFire() => energy.GetHealth() > fireCost;

    public void KeyDown()
    {
        if(CanUseFire())
        {
            UsingFire = true;
            if(add != null)
                energy.StopAdd(add);
            drain = energy.StartDrain();
            //transform.GetChild(0).gameObject.SetActive(true);
            for(int i = 0; i < transform.GetChild(1).childCount; i++)
            {
                transform.GetChild(1).GetChild(i).GetComponent<FireEffect>().Play();
            }
            
            
        }
    }

    private void Update()
    {
        if (UsingFire)
        {
            var bubble = Instantiate(dragonPihPrefab, attackProsition.position, Quaternion.identity);
            var coef = 1;
            bubble.GetComponent<DragonBreath>().lifeTime = attackBubleLifeTime;
            bubble.GetComponent<Rigidbody2D>().AddForce((transform.up + direction * coef) * speed);
        }
    }

    public void KeyUp()
    {
        if(UsingFire)
            add = energy.StartAddEnergy();
        UsingFire = false;
        if(drain != null)
            energy.StopDrain(drain);
        //transform.GetChild(0).gameObject.SetActive(false);
        for (int i = 0; i < transform.GetChild(1).childCount; i++)
        {
            transform.GetChild(1).GetChild(i).GetComponent<FireEffect>().Stop();
        }
    }
    
}
