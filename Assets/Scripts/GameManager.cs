﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance { get; private set; }

    public GameObject Target => GameObject.FindWithTag("eggShield");

    public float GameTime { get; private set; }
    public Action OnEndGame;
    private string name;
    public int Kills { get; private set; } = 0;

    public void Kill() => Kills++;
    public GameObject player
    {
        get
        {
            var player = GameObject.FindWithTag("Player");
            return player;
        }
    }

    public void NameChanged(string newText)
    {
        name = newText;
    }
    private void EndGame()
    {
        GameTime = (Time.timeSinceLevelLoad / 60);
        Files.Write(GameTime, Kills, name);
    }

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        //DontDestroyOnLoad(gameObject);

        InitializeManager();
    }

    private void InitializeManager()
    {
        OnEndGame += EndGame;
    }

    public Vector3 GetPlayerMouseWorldPos()
    {
        if (player == null)
        {
            return Vector3.zero;
        }

        Plane plane = new Plane(Vector3.up, player.transform.position);
        float distance;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 worldPosition = Vector3.zero;
        if (plane.Raycast(ray, out distance))
        {
            worldPosition = ray.GetPoint(distance);
        }
        return worldPosition;
    }

    public Vector3 GetPlayerDirToMouseWorldPos()
    {
        var mouseWorldPosition = GetPlayerMouseWorldPos();
        if (mouseWorldPosition == Vector3.zero || player == null)
        {
            // invalid world pos
            return Vector3.zero;
        }
        var dir = mouseWorldPosition - player.transform.position;
        dir.Normalize();
        return dir;
    }
}

