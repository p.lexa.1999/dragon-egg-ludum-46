﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

[RequireComponent(typeof(AIPath))]
[RequireComponent(typeof(Animator))]
public class PeasantAnimController : MonoBehaviour
{
    AIPath aiPath;
    Animator animator;
    HealthComponent health;

    Vector3 lastVelocity = Vector3.zero;

    void Start()
    {
        aiPath = GetComponent<AIPath>();
        animator = GetComponent<Animator>();
        health = GetComponent<HealthComponent>();
        if (health != null)
        {
            health.OnDeath += HandleDeath;
        }
    }

    void Update()
    {
        if (!aiPath.velocity.Equals(Vector3.zero))
        {
            lastVelocity = aiPath.velocity;
        }
        animator.SetFloat("xSpeed", lastVelocity.x);
        animator.SetFloat("ySpeed", lastVelocity.y);
        animator.SetFloat("xAtkDir", lastVelocity.x * 10);
        animator.SetFloat("yAtkDir", lastVelocity.y * 10);
    }

    public void PlayAttackAnim()
    {
        animator.SetTrigger("Attack");
    }

    private void HandleDeath(GameObject killer)
    {
        PlayDeathAnim();
        transform.GetChild(0).gameObject.SetActive(false);
    }

    AIDestinationSetter destSetter;

    public void PlayDeathAnim()
    {
        animator.Play("death");
        aiPath.enabled = false;
        StartCoroutine(DelayedDestroy());
    }

    private void OnDestroy()
    {
        if (health != null)
        {
            health.OnDeath -= HandleDeath;
        }
    }

    IEnumerator DelayedDestroy()
    {
        yield return new WaitForSeconds(5.0f);
        Destroy(this.gameObject);
    }

}
