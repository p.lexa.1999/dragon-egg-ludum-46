﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    MovementController mover;
    private DragonAttack attack;

    private void Start()
    {
        mover = GetComponent<MovementController>();
        attack = GetComponent<DragonAttack>();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Mouse0))
            attack.KeyDown();
        if(Input.GetKeyUp(KeyCode.Mouse0))
            attack.KeyUp();
    }

    private void FixedUpdate()
    {
        if (mover != null)
        {
            var horizontal = Input.GetAxis("Horizontal");
            var vertical = Input.GetAxis("Vertical");

            mover.direction = new Vector3(horizontal, vertical, 0);
        }
    }

}
