﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public Vector3 direction;
    InputController input;
    public float moveSpeed;
    [SerializeField] private float rotSpeed = 10.0f;
    private bool isSmoothRotationRequested = false;
    private Vector3 rotationRequestDirection = Vector3.zero;
    private float rotationRequestSpeed = 15f;
    public Quaternion desiredRotation;
    AnimatorController animator;

    void Start()
    {
        animator = GetComponent<AnimatorController>();
        input = GetComponent<InputController>();
    }

    
    void FixedUpdate()
    {
        animator.SetSpeed(direction.magnitude);

        if (direction.magnitude > 0.1)
        {
            var limitedVector = Vector3.ClampMagnitude(direction, 1);
            transform.position += limitedVector * Time.fixedDeltaTime * moveSpeed;
            RotationFixeedUpdate(direction, 3);
        }

    }
    

    private void RotationFixeedUpdate(Vector3 dir, float speed)
    {
        desiredRotation = Quaternion.LookRotation(Vector3.forward,dir);
        transform.rotation = Quaternion.Lerp(
            transform.rotation,
            desiredRotation,
            speed * Time.fixedDeltaTime
        );
    }

}
