﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmartLayerSystem : MonoBehaviour
{
    public List<GameObject> list;

    // Start is called before the first frame update
    void Start()
    {
        FindGameObjectsWithLayer(8);
        ChangeLayers();
    }

    private void ChangeLayers()
    {
        foreach(GameObject obj in list)
        {
            if (obj == null || obj.transform.Find("LayerHelper") == null) continue;

            Debug.Log(obj.transform.Find("LayerHelper").transform.position.y + " child " + obj.transform.position + " parent ");
            obj.GetComponent<SpriteRenderer>().sortingOrder = (int)(obj.transform.Find("LayerHelper").transform.position.y * -100);
        }
    }

    private void FindGameObjectsWithLayer(int layer){
         var goArray = FindObjectsOfType<GameObject>();
        list = new List<GameObject>();
        for (var i = 0; i<goArray.Length; i++) {
             if (goArray[i].layer == layer) {
                 list.Add(goArray[i]);
            }
        }
    }
}
