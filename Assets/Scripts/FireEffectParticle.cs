﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireEffectParticle : MonoBehaviour
{
    public float startSize;
    public float endSize;
    public Color startColor;
    public Color endColor;
    public float lifeTime;
    public float timeLeft;
    Renderer renderer;
    public Color currentColor;

    void Start()
    {
        timeLeft = 0;
        renderer = GetComponent<SpriteRenderer>();
        renderer.material.color = startColor;
        transform.localScale = Vector3.one * startSize;
    }

    // Update is called once per frame
    void Update()
    {
        if (timeLeft >= lifeTime)
        {
            transform.localScale = Vector3.one * endSize;
            renderer.material.color = endColor;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }
        else
        {
            transform.localScale = Vector3.Lerp(startSize * Vector3.one, endSize * Vector3.one, timeLeft / lifeTime);
            currentColor = Color.Lerp(startColor, endColor, timeLeft / lifeTime);
            renderer.material.color = currentColor;

            
            timeLeft += Time.deltaTime;
        }
    }
}
