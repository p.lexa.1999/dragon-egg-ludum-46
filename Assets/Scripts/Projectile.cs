﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float damage;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("eggShield"))
        {
            other.gameObject.GetComponent<HealthComponent>().TakeDamage(damage);
            Destroy(gameObject);
        }
    }
}
