﻿using System;
using UnityEngine;

public abstract class EnemySelectionBase : MonoBehaviour
{
    public abstract GameObject Enemy { get; }

    public bool HasEnemy => Enemy != null;
}