﻿using UnityEngine;

public class EnemySelection : EnemySelectionBase
{
    public GameObject targetOverride;

    public override GameObject Enemy
    {
        get
        {
            return GameManager.Instance.Target;
        }
    }
}
