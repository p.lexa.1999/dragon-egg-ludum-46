﻿using UnityEngine;

public class DragonBreath : MonoBehaviour
{
    public float lifeTime;
    private float timer;

    private void Start()
    {
        timer = lifeTime;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("enemy"))
        {
            other.gameObject.GetComponent<FireDOT>().onFire.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.gameObject.CompareTag("enemy"))
            other.gameObject.GetComponent<FireDOT>().EndFire();
    }

    private void Update()
    {
        if (timer<=0)
        {
            Destroy(gameObject);
        }

        timer -= Time.deltaTime;
    }
}
