﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerChanger : MonoBehaviour
{
    [SerializeField] GameObject backUI;
    [SerializeField] GameObject frontUI;
    SpriteRenderer sr;
    int newLayer = 0;

    private void Start()
    {
        sr = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (transform.Find("LayerHelper") != null)
        {
            newLayer = (int)(transform.Find("LayerHelper").transform.position.y * -100);
            if (!(sr.sortingOrder == newLayer))
            {
                if (backUI != null || frontUI != null)
                {
                    backUI.GetComponent<SpriteRenderer>().sortingOrder = newLayer;
                    frontUI.GetComponent<SpriteRenderer>().sortingOrder = newLayer + 1;
                }
            }
            sr.sortingOrder = newLayer;
        }
    }
}
