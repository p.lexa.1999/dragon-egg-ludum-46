﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;

public class HealthComponentBase : MonoBehaviour
{
    public event Action<GameObject> OnDeath;
    public event Action OnResurrect;
    public event Action<GameObject, float> OnHealthDecreased;
    public event Action<float> OnHealthIncreased;
    public event Action OnChanged;

    [SerializeField]
    private int initialHealth = 100;
    [SerializeField]
    private int maxHealth = 100;
    public int MaxHealth => maxHealth;

    private float health = 0;
    public float GetHealth() => health;

    public bool IsAlive => health > 0;
    public bool IsDead => !IsAlive;

    private bool _isInGodmode;
    public bool isInGodmode
    {
        get => _isInGodmode;
        set
        {
            _isInGodmode = value;
            OnChanged?.Invoke();
        }
    }

    private bool isRigidbodyKinematic;
    private bool isColliderTrigger;

    public void RestoreHealth(float amount)
    {
        Assert.IsTrue(amount >= 0, "Health restore amount can't be negative");
        Debug.Log("Restore health " + amount, this);
        ProcessHealthChange(amount);
    }

    public void TakeDamage(float damage, GameObject attacker = null)
    {
        Debug.Log("Taken damage " + damage + " from " + (attacker?.name ?? "null"), this);
        damage = PreProcessDamage(damage, attacker);
        SubstractHealth(damage, attacker);
    }

    public void Kill(GameObject killer = null)
    {
        Debug.Log("Kill", this);
        SubstractHealth(health, killer);
    }

    private void SubstractHealth(float amount, GameObject attacker)
    {
        Assert.IsTrue(amount >= 0, "Health substract amount can't be negative");

        if (isInGodmode)
        {
            Debug.Log("Godmode is active, skip substract health", this);
            return;
        }

        ProcessHealthChange(-amount, attacker);
    }

    private float PreProcessDamage(float damage, GameObject attacker)
    {
        // nothing to preprocess for now
        return damage;
    }

    private void ProcessHealthChange(float amount, GameObject healthChanger = null)
    {
        var prevHealth = health;
        health = Mathf.Clamp(prevHealth + amount, 0, MaxHealth);

        if (prevHealth == health) return;
        Debug.Log("Health change: prev - " + prevHealth + " current - " + health, this);
        OnChanged?.Invoke();

        var delta = Math.Abs(prevHealth - health);
        if (prevHealth > health)
        {
            OnHealthDecreased?.Invoke(healthChanger, delta);
            if (prevHealth > 0 && health == 0)
            {
                ProcessDeath(healthChanger);
            }
        }
        else if (prevHealth < health)
        {
            if (prevHealth == 0 && health > 0)
            {
                ProcessResurrect();
            }
            OnHealthIncreased?.Invoke(delta);
        }
    }

    protected virtual void ProcessDeath(GameObject killer)
    {
        var bhvTree = GetComponent<BhvTreeComponent>();
        bhvTree?.StopUpdating();

        if (gameObject.CompareTag("enemy"))
            GameManager.Instance.Kill();
        if(gameObject.CompareTag("eggShield"))
            GameManager.Instance.OnEndGame.Invoke();

        OnDeath?.Invoke(killer);
    }

    private void ProcessResurrect()
    {
        var bhvTree = GetComponent<BhvTreeComponent>();
        bhvTree?.StartUpdating();

        OnResurrect?.Invoke();
    }

    public virtual void Start()
    {
        if (initialHealth == 0)
        {
            ProcessDeath(null);
        } else
        {
            RestoreHealth(initialHealth);
        }
    }
}
