﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnergyBar : MonoBehaviour
{
    [SerializeField] private Transform bar;
    [SerializeField] private GameObject body;
    private EnergyComponent _energy;
    
    private void SetSize(float sizeNormalized) => bar.localScale = new Vector3(sizeNormalized, 1f);

    private void EnergyChange()
    {
        float currentEnergy = _energy.GetHealth();
        if (currentEnergy > 0)
        {
            var health = currentEnergy/_energy.MaxHealth;
            SetSize(health);
        }
        else 
            SetSize(0f);
    }
    
    private void Start()
    {
        _energy = body.GetComponent<EnergyComponent>();
        _energy.OnChanged += EnergyChange;
    }
}
