﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Transform bar;
    [SerializeField] private GameObject body;
    private HealthComponent _health;
    
    private void SetSize(float sizeNormalized) => bar.localScale = new Vector3(sizeNormalized, 1f);

    private void HealthChange()
    {
        float currentHealth = _health.GetHealth();
        if (currentHealth > 0)
        {
            var health = currentHealth / _health.MaxHealth;
            SetSize(health);
        }
        else 
            SetSize(0f);
    }
    
    private void Start()
    {
        _health = body.GetComponent<HealthComponent>();
        _health.OnChanged += HealthChange;
    }
}
