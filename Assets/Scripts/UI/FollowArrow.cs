﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowArrow : MonoBehaviour
{
    [SerializeField] private GameObject arrow;
    public bool Active { get; private set; }

    private void OnBecameInvisible()
    {
        if (arrow != null)
        {
            arrow.GetComponent<SpriteRenderer>().enabled = true;
        }
        Active = true;
    }

    private void OnBecameVisible()
    {
        Active = false;
        if (arrow != null)
        {
            arrow.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    
}
