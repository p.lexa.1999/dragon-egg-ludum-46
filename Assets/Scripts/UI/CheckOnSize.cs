﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckOnSize : MonoBehaviour
{
    private Vector3 min, max;
    [SerializeField] private GameObject egg;
    private FollowArrow fa;
    [SerializeField] private float forceSpeed;

    void Start ()
    {
        fa = egg.GetComponent<FollowArrow>();
    }

    private void Direction()
    {
        var dir = egg.transform.position - transform.position;
        var desiredRotation = Quaternion.LookRotation(Vector3.forward,-dir.normalized);
        transform.rotation = Quaternion.Lerp(
            transform.rotation,
            desiredRotation,
            10 * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("MainCamera"))
            forceSpeed = 0;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        forceSpeed = 50;
    }

    private void Update()
    {
        if(fa.Active)
        {
            Direction();
        var dir = egg.transform.position - transform.position;
        transform.position += dir.normalized * forceSpeed * Time.deltaTime;
        }
    }
}
