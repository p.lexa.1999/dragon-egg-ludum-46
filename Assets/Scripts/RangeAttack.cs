﻿using System;
using System.Collections;
using UnityEngine;

public class RangeAttack : MonoBehaviour
{
    [SerializeField] private GameObject projectile;
    [SerializeField] private int force;
    [SerializeField] private float damage = 2;

    public void TrowProjectile()
    {
        var arrow = SpawnProjectile();
        var dir = GameManager.Instance.Target.transform.position - arrow.transform.position;
        arrow.GetComponent<Rigidbody2D>().AddForceAtPosition(dir.normalized * force, arrow.transform.position);
    }

    private GameObject SpawnProjectile()
    {
        var dir = GameManager.Instance.Target.transform.position - transform.position;
        dir.Normalize();
        var angleZ = Vector3.SignedAngle(Vector3.up, dir, Vector3.forward);
        angleZ -= 90;
        var arrow = Instantiate(projectile, transform.position + dir, Quaternion.Euler(0, 0, angleZ));
        arrow.GetComponent<Projectile>().damage = damage;
        return arrow;
    }
}
