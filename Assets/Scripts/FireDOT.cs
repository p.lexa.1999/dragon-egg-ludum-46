﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireDOT : MonoBehaviour
{
    [SerializeField] private int damage;
    [SerializeField] private float perSec;
    [SerializeField] private float lifeTime;
    private float _currentTime;
    public Action onFire;
    private bool Fire { get; set; }
    private HealthComponent _health;

    private void StartFire() => Fire = true;
    public void EndFire() => Fire = false;

    private void Start()
    {
        _health = GetComponent<HealthComponent>();
        onFire += StartFire;
        onFire += FireStart;
    }

    private void FireStart() => StartCoroutine(DoTCoroutine());

    private IEnumerator DoTCoroutine()
    {
        _currentTime = lifeTime;
        while(Fire)
        {
            _health.TakeDamage(damage);
            yield return new WaitForSeconds(.1f);
        }
        while (_currentTime > 0)
        {
            _health.TakeDamage(damage);
            yield return new WaitForSeconds(perSec);
            _currentTime -= perSec;
        }
    }
    
}
